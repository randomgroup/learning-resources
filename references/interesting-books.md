 - **Deep Learning With PyTorch**, by Vishnu Subramanian [[google book]](https://books.google.com/books/about/Deep_Learning_with_PyTorch.html?hl=&id=DOlODwAAQBAJ)

 - **Deep Learning With Python**, by Francois Chollet [[google book]](https://books.google.com/books/about/Deep_Learning_with_Python.html?hl=&id=Yo3CAQAACAAJ)

 - **Machine Learning**, by Peter Flach [[google book]](https://market.android.com/details?id=book-A4d4CgAAQBAJ)

 - **Principles And Theory For Data Mining And Machine Learning**, by Bertrand Clarke, Ernest Fokoue, and Hao Helen Zhang [[google book]](https://market.android.com/details?id=book-RQHB4_p3bJoC)

 - **Introduction To Machine Learning**, by Ethem Alpaydin [[google book]](https://books.google.com/books/about/Introduction_to_Machine_Learning.html?hl=&id=NP5bBAAAQBAJ)

 - **Introducing Data Science**, by Davy Cielen, Arno Meysman, and Mohamed Ali [[google book]](https://books.google.com/books/about/Introducing_Data_Science.html?hl=&id=zYbisgEACAAJ)

 - **Mastering Machine Learning With Python In Six Steps**, by Manohar Swamynathan [[google book]](https://market.android.com/details?id=book--u4mDwAAQBAJ)

 - **An Introduction To Machine Learning**, by Miroslav Kubat [[google book]](https://books.google.com/books/about/Artificial_Intelligence.html?hl=&id=vABRDwAAQBAJ)

 - **An Introduction To Statistics With Python**, by Thomas Haslwanter [[google book]](https://market.android.com/details?id=book-ZBi1DAAAQBAJ)

 - **Information Theory And Statistical Learning**, by Frank Emmert-Streib, and Matthias Dehmer [[google book]](https://books.google.com/books/about/Information_Theory_and_Statistical_Learn.html?hl=&id=WGTF7daUvD4C)

 - **From Curve Fitting To Machine Learning**, by Achim Zielesny [[google book]](https://market.android.com/details?id=book-TG7JUVgVJUIC)

 - **Mathematical Tools For Data Mining**, by Dan Simovici, and Chabane Djeraba [[google book]](https://market.android.com/details?id=book-mue3BAAAQBAJ)

 - **Neural Networks And Deep Learning**, by Charu C. Aggarwal [[google book]](https://books.google.com/books/about/Neural_Networks_and_Deep_Learning.html?hl=&id=achqDwAAQBAJ)

 - **Artificial Intelligence**, by Stuart Jonathan Russell, and Peter Norvig [[google book]](https://books.google.com/books/about/Artificial_Intelligence.html?hl=&id=Cmf5cp4YBKMC)

 - **Probability For Statistics And Machine Learning**, by Anirban DasGupta [[google book]](https://market.android.com/details?id=book-nAa7PCTIBLkC)

 - **Introduction To Statistical Machine Learning**, by Masashi Sugiyama [[google book]](https://market.android.com/details?id=book-4YsdCAAAQBAJ)

 - **Introduction To Machine Learning With Python**, by Sarah Guido [[google book]](https://market.android.com/details?id=book-vbQlDQAAQBAJ)

 - **Modern Mathematical Statistics With Applications**, by Jay L. Devore, and Kenneth N. Berk [[google book]](https://market.android.com/details?id=book-5PRLUho-YYgC)

 - **Neurons**, by Masayoshi Hata [[google book]](https://books.google.com/books/about/Neurocytology.html?hl=&id=kl_URqA-B6EC)

 - **Statistical Learning From A Regression Perspective**, by Richard A. Berk [[google book]](https://market.android.com/details?id=book-D6ZlDQAAQBAJ)

 - **An Introduction To Statistical Learning**, by casa, home, et al. [[google book]](https://market.android.com/details?id=book-qcI_AAAAQBAJ)

 - **Pattern Recognition And Machine Learning**, by Christopher M. Bishop [[google book]](https://books.google.com/books/about/Pattern_Recognition_and_Machine_Learning.html?hl=&id=kOXDtAEACAAJ)

 - **Neural Networks And Statistical Learning**, by Ke-Lin Du, and M. N. S. Swamy [[google book]](https://market.android.com/details?id=book-wzK8BAAAQBAJ)

 - **Reinforcement Learning**, by Abhishek Nandy, and Manisha Biswas [[google book]](https://market.android.com/details?id=book-PwnrBwAAQBAJ)

 - **Practical Machine Learning With Python**, by Dipanjan Sarkar, Raghav Bali, and Tushar Sharma [[google book]](https://market.android.com/details?id=book-9ClEDwAAQBAJ)

 - **Python Machine Learning**, by Sebastian Raschka [[google book]](https://books.google.com/books/about/Python_Machine_Learning.html?hl=&id=GOVOCwAAQBAJ)

 - **Mathematical Statistics And Data Analysis**, by John A. Rice [[google book]](https://books.google.com/books/about/Mathematical_Statistics_and_Data_Analysi.html?hl=&id=EKA-yeX2GVgC)

 - **Principles Of Artificial Neural Networks**, by Daniel Graupe [[google book]](https://market.android.com/details?id=book-Zz27CgAAQBAJ)

 - **Proactive Data Mining With Decision Trees**, by casa, home, et al. [[google book]](https://market.android.com/details?id=book-Fey3BAAAQBAJ)

 - **Machine Learning In Python**, by Michael Bowles [[google book]](https://books.google.com/books/about/Learning_scikit_learn_Machine_Learning_i.html?hl=&id=OOotAgAAQBAJ)

 - **Computer Age Statistical Inference**, by Bradley Efron, and Trevor Hastie [[google book]](https://books.google.com/books/about/Computer_Age_Statistical_Inference.html?hl=&id=Sj1yDAAAQBAJ)

 - **Basic Principles And Applications Of Probability Theory**, by Valeriy Skorokhod [[google book]](https://market.android.com/details?id=book-dQkYMjRK3fYC)

 - **Data Science**, by Francesco Palumbo, Angela Montanari, and Maurizio Vichi [[google book]](https://books.google.com/books/about/Data_Science_from_Scratch.html?hl=&id=tRdqDwAAQBAJ)

 - **Counterfactuals And Causal Inference**, by Stephen L. Morgan, and Christopher Winship [[google book]](https://books.google.com/books/about/Counterfactuals_and_Causal_Inference.html?hl=&id=Q6YaBQAAQBAJ)

 - **Deep Learning In Natural Language Processing**, by Li Deng, and Yang Liu [[google book]](https://market.android.com/details?id=book-y_lcDwAAQBAJ)

 - **Probability Models**, by John Haigh [[google book]](https://market.android.com/details?id=book-6QhGAAAAQBAJ)

 - **Basics Of Modern Mathematical Statistics**, by Vladimir Spokoiny, and Thorsten Dickhaus [[google book]](https://market.android.com/details?id=book-e0cVBQAAQBAJ)

 - **Large Scale Machine Learning With Python**, by Bastiaan Sjardin, Luca Massaron, and Alberto Boschetti [[google book]](https://books.google.com/books/about/Large_Scale_Machine_Learning_with_Python.html?hl=&id=VwjVDQAAQBAJ)

 - **Data Structures And Algorithms With Python**, by Kent D. Lee, and Steve Hubbard [[google book]](https://market.android.com/details?id=book-jnEoBgAAQBAJ)

 - **Machine Learning In Complex Networks**, by Thiago Christiano Silva, and Liang Zhao [[google book]](https://market.android.com/details?id=book-cSd8CwAAQBAJ)

 - **Machine Learning**, by Sergios Theodoridis [[google book]](https://market.android.com/details?id=book-A4d4CgAAQBAJ)

 - **Machine Learning Models And Algorithms For Big Data Classification**, by Shan Suthaharan [[google book]](https://market.android.com/details?id=book-ad3HCgAAQBAJ)

 - **Learning From Data**, by Arthur M. Glenberg, and Matthew E. Andrzejewski [[google book]](https://books.google.com/books/about/Learning_from_Data.html?hl=&id=iZUzMwEACAAJ)

 - **Deep Learning With Python**, by Nikhil Ketkar [[google book]](https://books.google.com/books/about/Deep_Learning_with_Python.html?hl=&id=Yo3CAQAACAAJ)

 - **Machine Learning**, by Kevin P. Murphy [[google book]](https://market.android.com/details?id=book-A4d4CgAAQBAJ)

 - **Guide To Big Data Applications**, by S. Srinivasan [[google book]](https://market.android.com/details?id=book-zYQlDwAAQBAJ)

 - **Support Vector Machines**, by Ingo Steinwart, and Andreas Christmann [[google book]](https://books.google.com/books/about/Support_Vector_Machines_Theory_and_Appli.html?hl=&id=uTzMPJjVjsMC)

 - **Big Data Processing Using Spark In Cloud**, by casa, home, et al. [[google book]](https://market.android.com/details?id=book-wzFgDwAAQBAJ)

 - **Probabilistic Graphical Models**, by Luis Enrique Sucar [[google book]](https://books.google.com/books/about/Probabilistic_Graphical_Models.html?hl=&id=7dzpHCHzNQ4C)

 - **Python For Probability, Statistics, And Machine Learning**, by José Unpingco [[google book]](https://market.android.com/details?id=book-KAvNCwAAQBAJ)

 - **Convolutional Neural Networks In Visual Computing**, by Ragav Venkatesan, and Baoxin Li [[google book]](https://market.android.com/details?id=book-bAM7DwAAQBAJ)

 - **Introduction To Machine Learning**, by Ethem Alpaydin [[google book]](https://books.google.com/books/about/Introduction_to_Machine_Learning.html?hl=&id=NP5bBAAAQBAJ)

 - **Statistical Learning Theory And Stochastic Optimization**, by  [[google book]](https://books.google.com/books/about/Statistical_learning_theory_and_stochast.html?hl=&id=wtn_nxTfyu8C)

 - **Basic Elements Of Computational Statistics**, by Wolfgang Karl Härdle, Ostap Okhrin, and Yarema Okhrin [[google book]](https://market.android.com/details?id=book-JOI3DwAAQBAJ)

 - **Machine Learning With R**, by Brett Lantz [[google book]](https://market.android.com/details?id=book-buhPDwAAQBAJ)

 - **Introduction To Data Science**, by Laura Igual, and Santi Seguí [[google book]](https://market.android.com/details?id=book-w84wDgAAQBAJ)

 - **Scientific Inference**, by Simon Vaughan [[google book]](https://books.google.com/books/about/Scientific_Inference.html?hl=&id=a8oar96UFyQC)

 - **Markov Models For Pattern Recognition**, by Gernot A. Fink [[google book]](https://market.android.com/details?id=book-3c-4BAAAQBAJ)

 - **Introduction To Deep Learning**, by Sandro Skansi [[google book]](https://market.android.com/details?id=book-5cNKDwAAQBAJ)

 - **Bayesian Networks In R**, by Radhakrishnan Nagarajan, Marco Scutari, and Sophie Lèbre [[google book]](https://market.android.com/details?id=book-OCy5BAAAQBAJ)

 - **Numerical Analysis For Statisticians**, by Kenneth Lange [[google book]](https://market.android.com/details?id=book-va4_AAAAQBAJ)

 - **Python For Data Science For Dummies**, by Zanab Hussain, John Paul Mueller, and Luca Massaron [[google book]](https://books.google.com/books/about/Python_for_Data_Science_For_Dummies.html?hl=&id=ROHaCQAAQBAJ)

 - **Deep Learning**, by Adam Gibson, and Josh Patterson [[google book]](https://books.google.com/books/about/Deep_Learning.html?hl=&id=Np9SDQAAQBAJ)

 - **Applied Multivariate Statistics With R**, by Daniel Zelterman [[google book]](https://market.android.com/details?id=book-tsBOCgAAQBAJ)

 - **Statistical Inference**, by Helio S. Migon, Dani Gamerman, and Francisco Louzada [[google book]](https://books.google.com/books/about/Aspects_of_Statistical_Inference.html?hl=&id=MgF6a11WyMEC)

 - **Linear Models With R, Second Edition**, by Julian J. Faraway [[google book]](https://market.android.com/details?id=book-i0DOBQAAQBAJ)

 - **Kernel Methods And Machine Learning**, by S. Y. Kung [[google book]](https://books.google.com/books/about/Kernel_Methods_and_Machine_Learning.html?hl=&id=aRcmAwAAQBAJ)

 - **Statistical Reinforcement Learning**, by Masashi Sugiyama [[google book]](https://market.android.com/details?id=book-GncZBwAAQBAJ)

 - **Introduction To Probability With R**, by Kenneth Baclawski [[google book]](https://market.android.com/details?id=book-Kglc9g5IPf4C)

 - **Data Science In R**, by Deborah Nolan, and Duncan Temple Lang [[google book]](https://market.android.com/details?id=book-481NDgAAQBAJ)

 - **Regularization, Optimization, Kernels, And Support Vector Machines**, by Johan A.K. Suykens, Marco Signoretto, and Andreas Argyriou [[google book]](https://market.android.com/details?id=book-5Y_SBQAAQBAJ)

 - **Using R For Introductory Statistics, Second Edition**, by John Verzani [[google book]](https://books.google.com/books/about/Using_R_for_Introductory_Statistics_Seco.html?hl=&id=O86uAwAAQBAJ)

