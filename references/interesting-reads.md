- [How to use Facebook Graph API and extract data using Python!](https://medium.com/p/how-to-use-facebook-graph-api-and-extract-data-using-python-1839e19d6999)

- [5 Reasons “Logistic Regression” should be the first thing you learn when becoming a Data Scientist](https://medium.com/p/5-reasons-logistic-regression-should-be-the-first-thing-you-learn-when-become-a-data-scientist-fcaae46605c4)

- [Real-time and video processing object detection using Tensorflow, OpenCV and Docker.](https://medium.com/p/real-time-and-video-processing-object-detection-using-tensorflow-opencv-and-docker-2be1694726e5)

- [Clearing the Confusion: AI vs Machine Learning vs Deep Learning Differences](https://medium.com/p/clearing-the-confusion-ai-vs-machine-learning-vs-deep-learning-differences-fce69b21d5eb)

- [Quick Tip: The easiest way to grab data out of a web page in Python](https://medium.com/p/quick-tip-the-easiest-way-to-grab-data-out-of-a-web-page-in-python-7153cecfca58)

- [Let’s make a map! Using Geopandas, Pandas and Matplotlib to make a Choropleth map](https://medium.com/p/lets-make-a-map-using-geopandas-pandas-and-matplotlib-to-make-a-chloropleth-map-dddc31c1983d)

- [Neural Networks to Predict the Market – Towards Data Science](https://medium.com/p/neural-networks-to-predict-the-market-c4861b649371)

- [Reinforcement Learning from scratch – Insight Data](https://medium.com/p/reinforcement-learning-from-scratch-819b65f074d8)

- [Python Basics for Data Science – Towards Data Science](https://medium.com/p/python-basics-for-data-science-6a6c987f2755)

- [Understanding Descriptive Statistics – Towards Data Science](https://medium.com/p/understanding-descriptive-statistics-c9c2b0641291)

- [Understanding Boxplots – Towards Data Science](https://medium.com/p/understanding-boxplots-5e2df7bcbd51)

- [Interpretable Machine Learning with XGBoost – Towards Data Science](https://medium.com/p/interpretable-machine-learning-with-xgboost-9ec80d148d27)

- [Programming Skills, A Complete Roadmap for Learning Data Science — Part 1](https://medium.com/p/programming-skills-a-complete-roadmap-for-learning-data-science-part-1-7913b289751b)

- [‘I want to learn Artificial Intelligence and Machine Learning. Where can I start?’](https://medium.com/p/i-want-to-learn-artificial-intelligence-and-machine-learning-where-can-i-start-7a392a3086ec)

- [Outlier Detection with Isolation Forest – Towards Data Science](https://medium.com/p/outlier-detection-with-isolation-forest-3d190448d45e)

- [3 facts about time series forecasting that surprise experienced machine learning practitioners.](https://medium.com/p/3-facts-about-time-series-forecasting-that-surprise-experienced-machine-learning-practitioners-69c18ee89387)

- [Stop Installing Tensorflow using pip for performance sake!](https://medium.com/p/stop-installing-tensorflow-using-pip-for-performance-sake-5854f9d9eb0c)

- [How to use Dataset in TensorFlow – Towards Data Science](https://medium.com/p/how-to-use-dataset-in-tensorflow-c758ef9e4428)

- [Building A Deep Learning Model using Keras – Towards Data Science](https://medium.com/p/building-a-deep-learning-model-using-keras-1548ca149d37)

- [Version Control with Jupyter Notebooks – Towards Data Science](https://medium.com/p/version-control-with-jupyter-notebooks-f096f4d7035a)

- [Polynomial Regression – Towards Data Science](https://medium.com/p/polynomial-regression-bbe8b9d97491)

- [5 Free R Programming Courses for Data Scientists and ML Programmers](https://medium.com/p/5-free-r-programming-courses-for-data-scientists-and-ml-programmers-5732cb9e10)

- [Using Recurrent Neural Networks to Predict Bitcoin (BTC) Prices](https://medium.com/p/using-recurrent-neural-networks-to-predict-bitcoin-btc-prices-c4ff70f9f3e4)

- [Every single Machine Learning course on the internet, ranked by your reviews](https://medium.com/p/every-single-machine-learning-course-on-the-internet-ranked-by-your-reviews-3c4a7b8026c0)

- [Transfer Learning – Towards Data Science](https://medium.com/p/transfer-learning-946518f95666)

- [Here’s how you can get a 2–6x speed-up on your data pre-processing with Python](https://medium.com/p/heres-how-you-can-get-a-2-6x-speed-up-on-your-data-pre-processing-with-python-847887e63be5)

- [Data Visualization with Bokeh in Python, Part I: Getting Started](https://medium.com/p/data-visualization-with-bokeh-in-python-part-one-getting-started-a11655a467d4)

- [Neural Network Embeddings Explained – Towards Data Science](https://medium.com/p/neural-network-embeddings-explained-4d028e6f0526)

- [Getting started with Data science in 30 days with R programming : Day 1](https://medium.com/p/getting-started-with-data-science-in-30-days-with-r-programming-day-1-e59dd8d377e7)

- [DAY 10 :Dimensionality Reduction with PCA and t-SNE in R](https://medium.com/p/dimensionality-reduction-with-pca-and-t-sne-in-r-2715683819)

- [Time Series Anomaly Detection Algorithms – Stats and Bots](https://medium.com/p/time-series-anomaly-detection-algorithms-1cef5519aef2)

- [Deep Learning Tips and Tricks – Towards Data Science](https://medium.com/p/deep-learning-tips-and-tricks-1ef708ec5f53)

- [Custom Loss functions for Deep Learning: Predicting Home Values with Keras for R](https://medium.com/p/custom-loss-functions-for-deep-learning-predicting-home-values-with-keras-for-r-532c9e098d1f)

- [Generating Drake Rap Lyrics using Language Models and LSTMs](https://medium.com/p/generating-drake-rap-lyrics-using-language-models-and-lstms-8725d71b1b12)

- [How to build a Neural Network with Keras – Towards Data Science](https://medium.com/p/how-to-build-a-neural-network-with-keras-e8faa33d0ae4)

- [Deep Learning meets Physics: Restricted Boltzmann Machines Part I](https://medium.com/p/deep-learning-meets-physics-restricted-boltzmann-machines-part-i-6df5c4918c15)

- [Cheat Sheet of Machine Learning and Python (and Math) Cheat Sheets](https://medium.com/p/cheat-sheet-of-machine-learning-and-python-and-math-cheat-sheets-a4afe4e791b6)

- [How I used Python to find interesting people to follow on Medium](https://medium.com/p/how-i-used-python-to-find-interesting-people-on-medium-be9261b924b0)

- [Flight data visualisation with Pandas and Matplotlib](https://medium.com/p/flight-data-visualisation-with-pandas-and-matplotlib-ebbd13038647)

- [Scraping Flight Data with Python (example) – WanderData – Medium](https://medium.com/p/scraping-flight-data-with-python-example-163cd8bde59b)

- [Web Scraping Tutorial with Python: Tips and Tricks – Hacker Noon](https://medium.com/p/web-scraping-tutorial-with-python-tips-and-tricks-db070e70e071)

- [Using Deep Q-Learning in FIFA 18 to perfect the art of free-kicks](https://medium.com/p/using-deep-q-learning-in-fifa-18-to-perfect-the-art-of-free-kicks-f2e4e979ee66)

- [Must know Information Theory concepts in Deep Learning (AI)](https://medium.com/p/must-know-information-theory-concepts-in-deep-learning-ai-e54a5da9769d)

- [Unsupervised Learning with Python – Towards Data Science](https://medium.com/p/unsupervised-learning-with-python-173c51dc7f03)

- [Intuitively Understanding Convolutions for Deep Learning](https://medium.com/p/intuitively-understanding-convolutions-for-deep-learning-1f6f42faee1)

- [A Comprehensive Guide to the Grammar of Graphics for Effective Visualization of Multi-dimensional…](https://medium.com/p/a-comprehensive-guide-to-the-grammar-of-graphics-for-effective-visualization-of-multi-dimensional-1f92b4ed4149)

- [Another Machine Learning Walk-Through and a Challenge](https://medium.com/p/another-machine-learning-walk-through-and-a-challenge-8fae1e187a64)

- [Web Scraping, Regular Expressions, and Data Visualization: Doing it all in Python](https://medium.com/p/web-scraping-regular-expressions-and-data-visualization-doing-it-all-in-python-37a1aade7924)

- [Automated Feature Engineering in Python – Towards Data Science](https://medium.com/p/automated-feature-engineering-in-python-99baf11cc219)

- [Introduction to Bayesian Networks – Towards Data Science](https://medium.com/p/introduction-to-bayesian-networks-81031eeed94e)

- [Python vs (and) R for Data Science – Noteworthy - The Journal Blog](https://medium.com/p/python-vs-and-r-for-data-science-833b48ccc91d)

- [Machine Learning Kaggle Competition Part One: Getting Started](https://medium.com/p/machine-learning-kaggle-competition-part-one-getting-started-32fb9ff47426)

- [Data Science, Machine Learning and Artificial Intelligence for Art](https://medium.com/p/data-science-machine-learning-and-artificial-intelligence-for-art-1ac48c4fad41)

- [Data Science and Machine Learning Interview Questions](https://medium.com/p/data-science-and-machine-learning-interview-questions-3f6207cf040b)

- [How to build your own Neural Network from scratch in Python](https://medium.com/p/how-to-build-your-own-neural-network-from-scratch-in-python-68998a08e4f6)

- [Machine Learning Illuminates the Body’s Dark Matter](https://medium.com/p/pattern-computer-mark-anderson-larry-smarr-leroy-hood-9ee9dd2a687d)

- [Learn Blockchains by Building One – Hacker Noon](https://medium.com/p/learn-blockchains-by-building-one-117428612f46)

- [Essential Cheat Sheets for Machine Learning and Deep Learning Engineers](https://medium.com/p/essential-cheat-sheets-for-machine-learning-and-deep-learning-researchers-efb6a8ebd2e5)

- [30 Amazing Machine Learning Projects for the Past Year (v.2018)](https://medium.com/p/30-amazing-machine-learning-projects-for-the-past-year-v-2018-b853b8621ac7)

- [All of the Tools You Should Be Using – Robert Iannuzzi – Medium](https://medium.com/p/all-of-the-tools-you-should-be-using-c29c34afc932)

- [An A-Z of useful Python tricks – freeCodeCamp.org](https://medium.com/p/an-a-z-of-useful-python-tricks-b467524ee747)

- [How to use Git efficiently – freeCodeCamp.org](https://medium.com/p/how-to-use-git-efficiently-54320a236369)
